from flask import Flask, flash, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm 
from wtforms import SelectField


DB_NAME = "geodata.db"
app = Flask(__name__)
app.config['SECRET_KEY'] = "QyBGCIMtqH"
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
db= SQLAlchemy()
db.init_app(app)



class State(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    city = db.relationship('District', backref='state', lazy=True)

class District(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    state_id = db.Column(db.Integer, db.ForeignKey('state.id'), nullable=False)
    tehsil = db.relationship('Tehsil', backref='district', lazy=True)


class Tehsil(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    district_id = db.Column(db.Integer, db.ForeignKey('district.id'), nullable=False)
    village = db.relationship('Village', backref='tehsil', lazy=True)


class Village(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    tehsil_id = db.Column(db.Integer, db.ForeignKey('tehsil.id'), nullable=False)
    khewat = db.relationship('Khewat', backref='village', lazy=True)

class Khewat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    khewatnumber = db.Column(db.Integer)
    village_id = db.Column(db.Integer, db.ForeignKey('village.id'), nullable=False)
    murraba = db.relationship('Murraba', backref='khewat', lazy=True)

class Murraba(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    murrabanumber = db.Column(db.Integer)
    khewat_id = db.Column(db.Integer, db.ForeignKey('khewat.id'), nullable=False)
    khasara = db.relationship('Khasara', backref='murraba', lazy=True)

class Khasara(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    khasaranumber = db.Column(db.Integer)
    owner_name = db.Column(db.String(50))
    murraba_id = db.Column(db.Integer, db.ForeignKey('murraba.id'), nullable=False)


class Form(FlaskForm):
    state = SelectField('state', choices=[]) 
    district = SelectField('district', choices=[])
    tehsil = SelectField('tehsil', choices=[])
    village = SelectField('village', choices=[])
    khewat = SelectField('khewat', choices=[])
    murraba = SelectField('murraba', choices=[])
    khasara = SelectField('khasara', choices=[])

@app.route('/', methods=['GET','POST'])
def index():
    form = Form()
    states = State.query.all()
    form.state.choices =[('Select State','---')]+[(state.id, state.name) for state in states]
    if request.method == 'POST':
        khasara_id = request.form.get('khasara')
        if khasara_id:
            details = Khasara.query.filter_by(id=khasara_id).all()
            Obj = {}
            for obj in details:
                Obj['khasaranumber'] = obj.khasaranumber
                Obj['name'] = obj.owner_name
                    
            flash(f"Khasara Number:{Obj['khasaranumber']} and the owner name is {Obj['name']}","success")
        else:
            flash(f"feilds are empty","error")         
         
    return render_template('index.html', form=form)


@app.route("/api/v1/region")
def region():
    data = ''
    if 'state' in str(request.query_string):
        state_id=request.args.get('state')
        data = District.query.filter_by(state_id=state_id).all()
        Array = [{'id':-1,'name':'---'}]
        for obj in data:
            Obj = {}
            Obj['id'] = obj.id
            Obj['name'] = obj.name
            Array.append(Obj)

    
    if 'district' in str(request.query_string):
        district_id=request.args.get('district')
        data = Tehsil.query.filter_by(district_id=district_id).all()
        Array = [{'id':-1,'name':'---'}]
        for obj in data:
            Obj = {}
            Obj['id'] = obj.id
            Obj['name'] = obj.name
            Array.append(Obj)


    if 'tehsil' in str(request.query_string):
        tehsil_id=request.args.get('tehsil')
        data = Village.query.filter_by(tehsil_id=tehsil_id).all()
        Array = [{'id':-1,'name':'---'}]
        for obj in data:
            Obj = {}
            Obj['id'] = obj.id
            Obj['name'] = obj.name
            Array.append(Obj)


    if 'village' in str(request.query_string):
        village_id=request.args.get('village')
        data = Khewat.query.filter_by(village_id=village_id).all()
        Array = [{'id': -1,'name':'---'}]
        for obj in data:
            Obj = {}
            Obj['id'] = obj.id
            Obj['name'] = obj.khewatnumber
            Array.append(Obj)


    if 'khewat' in str(request.query_string):
        khewat_id=request.args.get('khewat')
        data = Murraba.query.filter_by(khewat_id=khewat_id).all()            
        Array = [{'id': -1,'name':'---'}]
        for obj in data:
            Obj = {}
            Obj['id'] = obj.id
            Obj['name'] = obj.murrabanumber
            Array.append(Obj)


    if 'murraba' in str(request.query_string):
        murraba_id=request.args.get('murraba')
        data = Khasara.query.filter_by(murraba_id=murraba_id).all()            
        Array = [{'id':-1,'name':'---'}]
        for obj in data:
            Obj = {}
            Obj['id'] = obj.id
            Obj['name'] = obj.khasaranumber
            Array.append(Obj)

    return jsonify({'data' : Array})




if __name__ == "__main__":
    app.run(debug=True)

